﻿namespace Movies
{
    partial class Welcome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_ADMIN = new System.Windows.Forms.Button();
            this.btn_USER = new System.Windows.Forms.Button();
            this.txt_text = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_ADMIN
            // 
            this.btn_ADMIN.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_ADMIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ADMIN.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_ADMIN.Location = new System.Drawing.Point(57, 225);
            this.btn_ADMIN.Name = "btn_ADMIN";
            this.btn_ADMIN.Size = new System.Drawing.Size(215, 70);
            this.btn_ADMIN.TabIndex = 0;
            this.btn_ADMIN.Text = "ADMIN";
            this.btn_ADMIN.UseVisualStyleBackColor = false;
            this.btn_ADMIN.Click += new System.EventHandler(this.btn_ADMIN_Click);
            // 
            // btn_USER
            // 
            this.btn_USER.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_USER.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_USER.Location = new System.Drawing.Point(321, 225);
            this.btn_USER.Name = "btn_USER";
            this.btn_USER.Size = new System.Drawing.Size(218, 70);
            this.btn_USER.TabIndex = 1;
            this.btn_USER.Text = "USER";
            this.btn_USER.UseVisualStyleBackColor = false;
            this.btn_USER.Click += new System.EventHandler(this.btn_USER_Click);
            // 
            // txt_text
            // 
            this.txt_text.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txt_text.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_text.Location = new System.Drawing.Point(57, 67);
            this.txt_text.Multiline = true;
            this.txt_text.Name = "txt_text";
            this.txt_text.Size = new System.Drawing.Size(482, 91);
            this.txt_text.TabIndex = 2;
            this.txt_text.Text = "Lights, camera, action—your next cinematic adventure awaits here!";
            this.txt_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_text.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Welcome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(605, 394);
            this.Controls.Add(this.txt_text);
            this.Controls.Add(this.btn_USER);
            this.Controls.Add(this.btn_ADMIN);
            this.Name = "Welcome";
            this.Text = "Welcome";
            this.Load += new System.EventHandler(this.Welcome_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_ADMIN;
        private System.Windows.Forms.Button btn_USER;
        private System.Windows.Forms.TextBox txt_text;
    }
}

