﻿using Movies.adminPanel;
using Movies.BL;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;

namespace Movies
{
    public partial class Admin : Form
    {
        public Admin()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Admin_Load(object sender, EventArgs e)
        {
            try
            {
                using (MoviesContext db = new MoviesContext())
                {
                    var query = db.Movies.Include(i => i.MoviesCategory)
                                .Include(i => i.Director)
                                .Select(i => new
                                {
                                    i.ID,
                                    i.Title,
                                    category = i.MoviesCategory.Name,
                                    director = i.Director.FirstName + " " + i.Director.LastName,
                                    i.DurationMinutes,
                                    i.YearOfRelease
                                }).ToList();
                    AdminDataGrid.AutoGenerateColumns = true;
                    AdminDataGrid.DataSource = query;
                }
            }catch (Exception ex)
            {
                MessageBox.Show("Error acured while Loading dataGrid: " + ex.Message, "Updating Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddCategoryNationality_Click(object sender, EventArgs e)
        {
            CategoryAndNatiolaties addCategoryNationality = new CategoryAndNatiolaties();
            addCategoryNationality.Show();
        }

        private void NewDirectorBTN_Click(object sender, EventArgs e)
        {
            NewDirecotr newDirector = new NewDirecotr();
            newDirector.Show();
        }

        private void AddMovieBTN_Click(object sender, EventArgs e)
        {
            NewMovie newMovie = new NewMovie();
            newMovie.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                using (MoviesContext db = new MoviesContext())
                {
                    var query = db.Movies.Include(i => i.MoviesCategory)
                                .Include(i => i.Director)
                                .Select(i => new
                                {
                                    i.ID,
                                    i.Title,
                                    category = i.MoviesCategory.Name,
                                    director = i.Director.FirstName + " " + i.Director.LastName,
                                    i.DurationMinutes,
                                    i.YearOfRelease
                                }).ToList();
                    AdminDataGrid.AutoGenerateColumns = true;
                    AdminDataGrid.DataSource = query;
                }
            }catch (Exception ex)
            {
                MessageBox.Show("Error acured while updating dataGrid: " + ex.Message, "Updating Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void editCategoryNationalityBTN_Click(object sender, EventArgs e)
        {

        }

        private void btn_GO_BACK_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}
