﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Movies
{
    public partial class Main_Page : Form
    {
        private string selectedGenre = null;
        private string selectedLanguage = null;
        private int? selectedYear = null;
        public Main_Page()
        {
            InitializeComponent();
            Load += Main_Page_Load;
            cb_Genre.SelectedIndexChanged += cb_Genre_SelectedIndexChanged;
            cb_Language.SelectedIndexChanged += cb_Language_SelectedIndexChanged;
            cb_Year.SelectedIndexChanged += cb_Year_SelectedIndexChanged;
            btn_Filter.Click += btn_Filter_Click;
            cb_Sortby.SelectedIndexChanged += cb_Sortby_SelectedIndexChanged;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
           
        }

        private void Main_Page_Load(object sender, EventArgs e)
        {
            using (Model1 db = new Model1())
            {
                var query = db.Movies.Include(xz => xz.MoviesCategory)
                                     .Include(y => y.Director)
                                     .Select(m => new
                                     {
                                         m.ID,
                                         m.Title,
                                         m.DurationMinutes,
                                         m.YearOfRelease,
                                         Category = m.MoviesCategory.Name,
                                         Director = m.Director.LastName
                                     }).ToList();

                dataGridView1.AutoGenerateColumns = false;
                dataGridView1.Columns.Clear();
                dataGridView1.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "ID", DataPropertyName = "ID" });
                dataGridView1.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Title", DataPropertyName = "Title" });
                dataGridView1.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Duration Minutes", DataPropertyName = "DurationMinutes" });
                dataGridView1.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Year of Release", DataPropertyName = "YearOfRelease" });
                dataGridView1.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Category", DataPropertyName = "Category" });
                dataGridView1.Columns.Add(new DataGridViewTextBoxColumn { HeaderText = "Director", DataPropertyName = "Director" });
                dataGridView1.DataSource = query;
                cb_Sortby.Items.AddRange(new string[] { "Title", "Duration", "Year", "Category", "Director" });

                cb_Genre.Items.Add("Genre");
                cb_Genre.SelectedIndex = 0;

                cb_Language.Items.Add("Language");
                cb_Language.SelectedIndex = 0;

                cb_Year.Items.Add("Year");
                cb_Year.SelectedIndex = 0;

                var genres = db.MoviesCategories.Select(c => c.Name).Distinct().ToList();
                cb_Genre.Items.AddRange(genres.ToArray());
                selectedGenre = genres.FirstOrDefault();

                var languages = db.Nationalities.Select(v => v.Name).Distinct().ToList();
                cb_Language.Items.AddRange(languages.ToArray());
                selectedLanguage = languages.FirstOrDefault();

                var years = db.Movies.Select(m => m.YearOfRelease).Distinct().OrderBy(y => y).ToList();
                cb_Year.Items.AddRange(years.Select(y => y.ToString()).ToArray());
                selectedYear = years.FirstOrDefault();


                if (cb_Genre.Items.Count > 0)
                    cb_Genre.SelectedIndex = 0;
                if (cb_Language.Items.Count > 0)
                    cb_Language.SelectedIndex = 0;
                if (cb_Year.Items.Count > 0)
                    cb_Year.SelectedIndex = 0;
            }
        }

        private void cb_Genre_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedGenre = cb_Genre.SelectedItem.ToString();
        }

        private void cb_Language_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedLanguage = cb_Language.SelectedItem.ToString(); 
        }
        private void cb_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_Year.SelectedItem != null)
            {
                if (int.TryParse(cb_Year.SelectedItem.ToString(), out int year))
                {
                    selectedYear = year;
                }
                else
                {
                    selectedYear = null;
                }
            }
            else
            {
                selectedYear = null;
            }
        }

        private void btn_Filter_Click(object sender, EventArgs e)
        {
            using (var db = new Model1())
            {
                string genreFilter = selectedGenre == "Genre" ? null : selectedGenre;
                string languageFilter = selectedLanguage == "Language" ? null : selectedLanguage;
                int? yearFilter = selectedYear == null ? null : selectedYear;

                var filteredMovies = db.Movies
                          .Include(x => x.MoviesCategory)
                          .Include(y => y.Director)
                          .Where(m => (string.IsNullOrEmpty(genreFilter) || m.MoviesCategory.Name == genreFilter) &&
                                      (string.IsNullOrEmpty(languageFilter) || m.Director.Nationality.Name == languageFilter) &&
                                      (!yearFilter.HasValue || m.YearOfRelease == yearFilter))
                          .ToList();

                DisplayMovies(filteredMovies);
            }
        }

        private void DisplayMovies(List<Movy> filteredMovies)
        { 
            string sortBy = cb_Sortby.SelectedIndex.ToString();

            switch (sortBy)
            {
                case "Title":
                    filteredMovies = filteredMovies.OrderBy(m => m.Title).ToList();
                    break;
                case "Duration":
                    filteredMovies = filteredMovies.OrderBy(m => m.DurationMinutes).ToList();
                    break;
                case "Year":
                    filteredMovies = filteredMovies.OrderBy(m => m.YearOfRelease).ToList();
                    break;
                case "Category":
                    filteredMovies = filteredMovies.OrderBy(m => m.MoviesCategory.Name).ToList();
                    break;
                case "Director":
                    filteredMovies = filteredMovies.OrderBy(m => m.Director.LastName).ToList();
                    break;
                default:
                    filteredMovies = filteredMovies.OrderBy(m => m.Title).ToList();
                    break;
            }
            dataGridView1.DataSource = filteredMovies;
        }

        private void cb_Sortby_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dataGridView1.DataSource != null && dataGridView1.DataSource is List<Movy> filteredMovies)
            {
            
                DisplayMovies(filteredMovies);
            }
        }
    }
}