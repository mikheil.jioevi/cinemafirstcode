﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Movies.adminPanel.Delete
{
    public partial class DeleteCategoryNationality : Form
    {
        public DeleteCategoryNationality()
        {
            InitializeComponent();
        }

        private void DeleteCategoryNationality_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'cinemaDataSet.Movies' table. You can move, or remove it, as needed.
            // TODO: This line of code loads data into the 'directors._Directors' table. You can move, or remove it, as needed.
            this.directorsTableAdapter.Fill(this.directors._Directors);
            // TODO: This line of code loads data into the 'nationalities._Nationalities' table. You can move, or remove it, as needed.
            this.nationalitiesTableAdapter.Fill(this.nationalities._Nationalities);
            // TODO: This line of code loads data into the 'categoires.MoviesCategories' table. You can move, or remove it, as needed.
            this.moviesCategoriesTableAdapter.Fill(this.categoires.MoviesCategories);

        }
    }
}
