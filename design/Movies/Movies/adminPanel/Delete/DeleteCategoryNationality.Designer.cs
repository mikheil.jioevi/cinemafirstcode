﻿namespace Movies.adminPanel.Delete
{
    partial class DeleteCategoryNationality
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.categoires = new Movies.Datasets.Categoires();
            this.moviesCategoriesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.moviesCategoriesTableAdapter = new Movies.Datasets.CategoiresTableAdapters.MoviesCategoriesTableAdapter();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nationalities = new Movies.Datasets.Nationalities();
            this.nationalitiesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nationalitiesTableAdapter = new Movies.Datasets.NationalitiesTableAdapters.NationalitiesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.categoires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moviesCategoriesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nationalities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nationalitiesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.DataSource = this.moviesCategoriesBindingSource;
            this.listBox1.DisplayMember = "Name";
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 68);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 95);
            this.listBox1.TabIndex = 0;
            this.listBox1.ValueMember = "ID";
            // 
            // categoires
            // 
            this.categoires.DataSetName = "Categoires";
            this.categoires.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // moviesCategoriesBindingSource
            // 
            this.moviesCategoriesBindingSource.DataMember = "MoviesCategories";
            this.moviesCategoriesBindingSource.DataSource = this.categoires;
            // 
            // moviesCategoriesTableAdapter
            // 
            this.moviesCategoriesTableAdapter.ClearBeforeFill = true;
            // 
            // listBox2
            // 
            this.listBox2.DataSource = this.moviesCategoriesBindingSource;
            this.listBox2.DisplayMember = "Name";
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(309, 68);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(120, 95);
            this.listBox2.TabIndex = 1;
            this.listBox2.ValueMember = "ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(306, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            // 
            // nationalities
            // 
            this.nationalities.DataSetName = "Nationalities";
            this.nationalities.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // nationalitiesBindingSource
            // 
            this.nationalitiesBindingSource.DataMember = "Nationalities";
            this.nationalitiesBindingSource.DataSource = this.nationalities;
            // 
            // nationalitiesTableAdapter
            // 
            this.nationalitiesTableAdapter.ClearBeforeFill = true;
            // 
            // DeleteCategoryNationality
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Name = "DeleteCategoryNationality";
            this.Text = "DeleteCategoryNationality";
            this.Load += new System.EventHandler(this.DeleteCategoryNationality_Load);
            ((System.ComponentModel.ISupportInitialize)(this.categoires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moviesCategoriesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nationalities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nationalitiesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private Datasets.Categoires categoires;
        private System.Windows.Forms.BindingSource moviesCategoriesBindingSource;
        private Datasets.CategoiresTableAdapters.MoviesCategoriesTableAdapter moviesCategoriesTableAdapter;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Datasets.Nationalities nationalities;
        private System.Windows.Forms.BindingSource nationalitiesBindingSource;
        private Datasets.NationalitiesTableAdapters.NationalitiesTableAdapter nationalitiesTableAdapter;
    }
}