﻿using Movies.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Movies.adminPanel
{
    public partial class CategoryAndNatiolaties : Form
    {
        public CategoryAndNatiolaties()
        {
            InitializeComponent();
        }

        private void CategoryAndNatiolaties_Load(object sender, EventArgs e)
        {

        }

        private void AddNewCategoryBTN_Click(object sender, EventArgs e)
        {
            try
            {
                string categoryText = addNewCategoryTXT.Text;

                if (string.IsNullOrEmpty(categoryText))
                {
                    MessageBox.Show("Category can't be empty, Please enter valid text!", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                using (MoviesContext db = new MoviesContext())
                {
                    var category = new MoviesCategory { Name = categoryText };
                    db.MoviesCategories.Add(category);
                    db.SaveChanges();
                }

                MessageBox.Show("Category added successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error accured while adding new category: " + ex.Message, "adding error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void newNationalityBTN_Click(object sender, EventArgs e)
        {
            try
            {
                string newNationality = newNationaliryTXT.Text;

                if (string.IsNullOrEmpty(newNationality))
                {
                    MessageBox.Show("nationality can't be empty, Please enter valid text!", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                using (MoviesContext db = new MoviesContext())
                {
                    var nationality = new Nationality { Name = newNationality };
                    db.Nationalities.Add(nationality);
                    db.SaveChanges();
                }

                MessageBox.Show("Nationality added successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex) {
                MessageBox.Show("Error accured while adding new category: " + ex.Message, "adding error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
