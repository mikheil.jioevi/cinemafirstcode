﻿

using Movies.Datasets;

namespace Movies.adminPanel
{
    partial class NewDirecotr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nationalityList = new System.Windows.Forms.ListBox();
            this.nationalitiesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.nationalitiesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.firstnameTXT = new System.Windows.Forms.TextBox();
            this.lastnameTXT = new System.Windows.Forms.TextBox();
            this.NewDirectorBTN = new System.Windows.Forms.Button();
            this.nationalities = new Movies.Nationalities();
            this.nationalitiesBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.nationalitiesTableAdapter = new Movies.Datasets.NationalitiesTableAdapters.NationalitiesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.nationalitiesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nationalitiesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nationalities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nationalitiesBindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // nationalityList
            // 
            this.nationalityList.DataSource = this.nationalitiesBindingSource2;
            this.nationalityList.DisplayMember = "Name";
            this.nationalityList.FormattingEnabled = true;
            this.nationalityList.Location = new System.Drawing.Point(334, 98);
            this.nationalityList.Name = "nationalityList";
            this.nationalityList.Size = new System.Drawing.Size(162, 134);
            this.nationalityList.TabIndex = 0;
            this.nationalityList.ValueMember = "ID";
            // 
            // nationalitiesBindingSource
            // 
            this.nationalitiesBindingSource.DataMember = "Nationalities";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 36);
            this.label1.TabIndex = 1;
            this.label1.Text = "Firstname";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(614, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 36);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lastname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(328, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 36);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nationality";
            // 
            // firstnameTXT
            // 
            this.firstnameTXT.Location = new System.Drawing.Point(26, 98);
            this.firstnameTXT.Multiline = true;
            this.firstnameTXT.Name = "firstnameTXT";
            this.firstnameTXT.Size = new System.Drawing.Size(177, 38);
            this.firstnameTXT.TabIndex = 4;
            // 
            // lastnameTXT
            // 
            this.lastnameTXT.Location = new System.Drawing.Point(600, 98);
            this.lastnameTXT.Multiline = true;
            this.lastnameTXT.Name = "lastnameTXT";
            this.lastnameTXT.Size = new System.Drawing.Size(177, 38);
            this.lastnameTXT.TabIndex = 5;
            // 
            // NewDirectorBTN
            // 
            this.NewDirectorBTN.Location = new System.Drawing.Point(334, 290);
            this.NewDirectorBTN.Name = "NewDirectorBTN";
            this.NewDirectorBTN.Size = new System.Drawing.Size(162, 58);
            this.NewDirectorBTN.TabIndex = 6;
            this.NewDirectorBTN.Text = "ADD";
            this.NewDirectorBTN.UseVisualStyleBackColor = true;
            this.NewDirectorBTN.Click += new System.EventHandler(this.NewDirectorBTN_Click);
            // 
            // nationalities
            // 
            this.nationalities.DataSetName = "Nationalities";
            this.nationalities.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // nationalitiesBindingSource2
            // 
            this.nationalitiesBindingSource2.DataMember = "Nationalities";
            this.nationalitiesBindingSource2.DataSource = this.nationalities;
            // 
            // nationalitiesTableAdapter
            // 
            this.nationalitiesTableAdapter.ClearBeforeFill = true;
            // 
            // NewDirecotr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.NewDirectorBTN);
            this.Controls.Add(this.lastnameTXT);
            this.Controls.Add(this.firstnameTXT);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nationalityList);
            this.Name = "NewDirecotr";
            this.Text = "NewDirecotr";
            this.Load += new System.EventHandler(this.NewDirecotr_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nationalitiesBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nationalitiesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nationalities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nationalitiesBindingSource2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox nationalityList;
        private System.Windows.Forms.BindingSource nationalitiesBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox firstnameTXT;
        private System.Windows.Forms.TextBox lastnameTXT;
        private System.Windows.Forms.Button NewDirectorBTN;
        private System.Windows.Forms.BindingSource nationalitiesBindingSource1;
        private Nationalities nationalities;
        private System.Windows.Forms.BindingSource nationalitiesBindingSource2;
        private Datasets.NationalitiesTableAdapters.NationalitiesTableAdapter nationalitiesTableAdapter;
    }
}