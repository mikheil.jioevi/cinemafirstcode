﻿using Movies.BL;
using System;
using System.Windows.Forms;

namespace Movies.adminPanel
{
    public partial class NewDirecotr : Form
    {
        public NewDirecotr()
        {
            InitializeComponent();
        }

        private void NewDirecotr_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'nationalities._Nationalities' table. You can move, or remove it, as needed.
            try
            {
                this.nationalitiesTableAdapter.Fill(this.nationalities._Nationalities);
       
            }catch
            (Exception ex)
            {
                MessageBox.Show("An error occurred while loading data: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void NewDirectorBTN_Click(object sender, EventArgs e)
        {
            try
            {
                string firstname = firstnameTXT.Text;
                string lastname = lastnameTXT.Text;
                int nationalityID = int.Parse(nationalityList.SelectedValue.ToString());

                if (string.IsNullOrEmpty(firstname))
                {
                    MessageBox.Show("Firstname field cann't be empty! please enter Firstname!", "validation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (string.IsNullOrEmpty(lastname))
                {
                    MessageBox.Show("Lastname field cann't be empty! please enter Lastname", "validation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                using (MoviesContext db = new MoviesContext())
                {
                    var director = new Director
                    {
                        FirstName = firstname,
                        LastName = lastname,
                        NationalityID = nationalityID,
                    };

                    db.Directors.Add(director);
                    db.SaveChanges();
                }

                MessageBox.Show("Director added successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while adding the Director: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
