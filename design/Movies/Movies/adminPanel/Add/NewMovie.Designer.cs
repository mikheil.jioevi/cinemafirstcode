﻿namespace Movies.adminPanel
{
    partial class NewMovie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.titleTXT = new System.Windows.Forms.TextBox();
            this.DirectorsList = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.durationTXT = new System.Windows.Forms.TextBox();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.newMovieBTN = new System.Windows.Forms.Button();
            this.CategoriesList = new System.Windows.Forms.ListBox();
            this.moviesCategoriesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoires = new Movies.Datasets.Categoires();
            this.moviesCategoriesTableAdapter = new Movies.Datasets.CategoiresTableAdapters.MoviesCategoriesTableAdapter();
            this.directors = new Movies.Datasets.Directors();
            this.directorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.directorsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.directorsTableAdapter = new Movies.Datasets.DirectorsTableAdapters.DirectorsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.moviesCategoriesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.directors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.directorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.directorsBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "TITLE";
            // 
            // titleTXT
            // 
            this.titleTXT.Location = new System.Drawing.Point(13, 68);
            this.titleTXT.Name = "titleTXT";
            this.titleTXT.Size = new System.Drawing.Size(100, 20);
            this.titleTXT.TabIndex = 1;
            // 
            // DirectorsList
            // 
            this.DirectorsList.DataSource = this.directorsBindingSource1;
            this.DirectorsList.DisplayMember = "LastName";
            this.DirectorsList.FormattingEnabled = true;
            this.DirectorsList.Location = new System.Drawing.Point(356, 68);
            this.DirectorsList.Name = "DirectorsList";
            this.DirectorsList.Size = new System.Drawing.Size(120, 95);
            this.DirectorsList.TabIndex = 3;
            this.DirectorsList.ValueMember = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(549, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Duration (MIN)";
            // 
            // durationTXT
            // 
            this.durationTXT.Location = new System.Drawing.Point(540, 68);
            this.durationTXT.Name = "durationTXT";
            this.durationTXT.Size = new System.Drawing.Size(100, 20);
            this.durationTXT.TabIndex = 5;
            // 
            // datePicker
            // 
            this.datePicker.CustomFormat = "yyyy/MM/dd";
            this.datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datePicker.Location = new System.Drawing.Point(552, 143);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(97, 20);
            this.datePicker.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(205, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Category";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(387, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Director";
            // 
            // newMovieBTN
            // 
            this.newMovieBTN.Location = new System.Drawing.Point(208, 237);
            this.newMovieBTN.Name = "newMovieBTN";
            this.newMovieBTN.Size = new System.Drawing.Size(367, 102);
            this.newMovieBTN.TabIndex = 9;
            this.newMovieBTN.Text = "Add";
            this.newMovieBTN.UseVisualStyleBackColor = true;
            this.newMovieBTN.Click += new System.EventHandler(this.newMovieBTN_Click);
            // 
            // CategoriesList
            // 
            this.CategoriesList.DataSource = this.moviesCategoriesBindingSource;
            this.CategoriesList.DisplayMember = "Name";
            this.CategoriesList.FormattingEnabled = true;
            this.CategoriesList.Location = new System.Drawing.Point(173, 68);
            this.CategoriesList.Name = "CategoriesList";
            this.CategoriesList.Size = new System.Drawing.Size(120, 95);
            this.CategoriesList.TabIndex = 2;
            this.CategoriesList.ValueMember = "ID";
            // 
            // moviesCategoriesBindingSource
            // 
            this.moviesCategoriesBindingSource.DataMember = "MoviesCategories";
            this.moviesCategoriesBindingSource.DataSource = this.categoires;
            // 
            // categoires
            // 
            this.categoires.DataSetName = "Categoires";
            this.categoires.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // moviesCategoriesTableAdapter
            // 
            this.moviesCategoriesTableAdapter.ClearBeforeFill = true;
            // 
            // directors
            // 
            this.directors.DataSetName = "Directors";
            this.directors.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // directorsBindingSource
            // 
            this.directorsBindingSource.DataSource = this.directors;
            this.directorsBindingSource.Position = 0;
            // 
            // directorsBindingSource1
            // 
            this.directorsBindingSource1.DataMember = "Directors";
            this.directorsBindingSource1.DataSource = this.directorsBindingSource;
            // 
            // directorsTableAdapter
            // 
            this.directorsTableAdapter.ClearBeforeFill = true;
            // 
            // NewMovie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.newMovieBTN);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.datePicker);
            this.Controls.Add(this.durationTXT);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DirectorsList);
            this.Controls.Add(this.CategoriesList);
            this.Controls.Add(this.titleTXT);
            this.Controls.Add(this.label1);
            this.Name = "NewMovie";
            this.Text = "NewMovie";
            this.Load += new System.EventHandler(this.NewMovie_Load);
            ((System.ComponentModel.ISupportInitialize)(this.moviesCategoriesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.directors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.directorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.directorsBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox titleTXT;
     
        private System.Windows.Forms.ListBox DirectorsList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox durationTXT;
        private System.Windows.Forms.DateTimePicker datePicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button newMovieBTN;
        private System.Windows.Forms.ListBox CategoriesList;
        private System.Windows.Forms.BindingSource moviesCategoriesBindingSource;
        private Datasets.Categoires categoires;
        private Datasets.CategoiresTableAdapters.MoviesCategoriesTableAdapter moviesCategoriesTableAdapter;
        private System.Windows.Forms.BindingSource directorsBindingSource;
        private Datasets.Directors directors;
        private System.Windows.Forms.BindingSource directorsBindingSource1;
        private Datasets.DirectorsTableAdapters.DirectorsTableAdapter directorsTableAdapter;
    }
}