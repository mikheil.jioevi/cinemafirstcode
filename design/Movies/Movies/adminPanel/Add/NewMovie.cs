﻿using Movies.BL;
using System;
using System.Windows.Forms;

namespace Movies.adminPanel
{
    public partial class NewMovie : Form
    {
        public NewMovie()
        {
            InitializeComponent();
        }

        private void NewMovie_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'directors._Directors' table. You can move, or remove it, as needed.
            this.directorsTableAdapter.Fill(this.directors._Directors);
            // TODO: This line of code loads data into the 'directors._Directors' table. You can move, or remove it, as needed.
            this.directorsTableAdapter.Fill(this.directors._Directors);
            // TODO: This line of code loads data into the 'categoires.MoviesCategories' table. You can move, or remove it, as needed.
            this.moviesCategoriesTableAdapter.Fill(this.categoires.MoviesCategories);
            // TODO: This line of code loads data into the 'categoires.MoviesCategories' table. You can move, or remove it, as needed.
            this.moviesCategoriesTableAdapter.Fill(this.categoires.MoviesCategories);
            // TODO: This line of code loads data into the 'categoires.MoviesCategories' table. You can move, or remove it, as needed.
            this.moviesCategoriesTableAdapter.Fill(this.categoires.MoviesCategories);
            try
            {
                // TODO: This line of code loads data into the 'cinemaDataSet2.Directors' table. You can move, or remove it, as needed.
                // TODO: This line of code loads data into the 'cinemaDataSet1.MoviesCategories' table. You can move, or remove it, as needed.
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while loading data: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void newMovieBTN_Click(object sender, EventArgs e)
        {
            try
            {
                string title = titleTXT.Text;
                if (string.IsNullOrWhiteSpace(title))
                {
                    MessageBox.Show("Title cannot be empty.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (!int.TryParse(durationTXT.Text, out int duration))
                {
                    MessageBox.Show("Invalid duration. Please enter a valid number.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (CategoriesList.SelectedValue == null || !int.TryParse(CategoriesList.SelectedValue.ToString(), out int category))
                {
                    MessageBox.Show("Please select a valid category.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (DirectorsList.SelectedValue == null || !int.TryParse(DirectorsList.SelectedValue.ToString(), out int director))
                {
                    MessageBox.Show("Please select a valid director.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                using (MoviesContext db = new MoviesContext())
                {
                    var movie = new Movy
                    {
                        MovieCategoryID = category,
                        Title = title,
                        DirectorID = director,
                        DurationMinutes = duration,
                        YearOfRelease = datePicker.Value.Date,
                    };

                    db.Movies.Add(movie);
                    db.SaveChanges();
                }

                MessageBox.Show("Movie added successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while adding the movie: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
