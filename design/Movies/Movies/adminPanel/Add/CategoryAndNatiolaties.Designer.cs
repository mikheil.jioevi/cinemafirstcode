﻿namespace Movies.adminPanel
{
    partial class CategoryAndNatiolaties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.addNewCategoryTXT = new System.Windows.Forms.TextBox();
            this.AddNewCategoryBTN = new System.Windows.Forms.Button();
            this.newNationalityBTN = new System.Windows.Forms.Button();
            this.newNationaliryTXT = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(45, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(249, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add new cateogry";
            // 
            // addNewCategoryTXT
            // 
            this.addNewCategoryTXT.Font = new System.Drawing.Font("Microsoft YaHei UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addNewCategoryTXT.Location = new System.Drawing.Point(51, 99);
            this.addNewCategoryTXT.Multiline = true;
            this.addNewCategoryTXT.Name = "addNewCategoryTXT";
            this.addNewCategoryTXT.Size = new System.Drawing.Size(243, 40);
            this.addNewCategoryTXT.TabIndex = 1;
            // 
            // AddNewCategoryBTN
            // 
            this.AddNewCategoryBTN.Font = new System.Drawing.Font("Microsoft YaHei UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddNewCategoryBTN.Location = new System.Drawing.Point(51, 165);
            this.AddNewCategoryBTN.Name = "AddNewCategoryBTN";
            this.AddNewCategoryBTN.Size = new System.Drawing.Size(243, 52);
            this.AddNewCategoryBTN.TabIndex = 2;
            this.AddNewCategoryBTN.Text = "Add\r\n";
            this.AddNewCategoryBTN.UseVisualStyleBackColor = true;
            this.AddNewCategoryBTN.Click += new System.EventHandler(this.AddNewCategoryBTN_Click);
            // 
            // newNationalityBTN
            // 
            this.newNationalityBTN.Font = new System.Drawing.Font("Microsoft YaHei UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newNationalityBTN.Location = new System.Drawing.Point(484, 165);
            this.newNationalityBTN.Name = "newNationalityBTN";
            this.newNationalityBTN.Size = new System.Drawing.Size(243, 52);
            this.newNationalityBTN.TabIndex = 5;
            this.newNationalityBTN.Text = "Add\r\n";
            this.newNationalityBTN.UseVisualStyleBackColor = true;
            this.newNationalityBTN.Click += new System.EventHandler(this.newNationalityBTN_Click);
            // 
            // newNationaliryTXT
            // 
            this.newNationaliryTXT.Font = new System.Drawing.Font("Microsoft YaHei UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newNationaliryTXT.Location = new System.Drawing.Point(484, 99);
            this.newNationaliryTXT.Multiline = true;
            this.newNationaliryTXT.Name = "newNationaliryTXT";
            this.newNationaliryTXT.Size = new System.Drawing.Size(243, 40);
            this.newNationaliryTXT.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(478, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(278, 35);
            this.label2.TabIndex = 3;
            this.label2.Text = "Add new Nationality";
            // 
            // CategoryAndNatiolaties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.newNationalityBTN);
            this.Controls.Add(this.newNationaliryTXT);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.AddNewCategoryBTN);
            this.Controls.Add(this.addNewCategoryTXT);
            this.Controls.Add(this.label1);
            this.Name = "CategoryAndNatiolaties";
            this.Text = "CategoryAndNatiolaties";
            this.Load += new System.EventHandler(this.CategoryAndNatiolaties_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox addNewCategoryTXT;
        private System.Windows.Forms.Button AddNewCategoryBTN;
        private System.Windows.Forms.Button newNationalityBTN;
        private System.Windows.Forms.TextBox newNationaliryTXT;
        private System.Windows.Forms.Label label2;
    }
}