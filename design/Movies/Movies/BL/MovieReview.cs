namespace Movies.BL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MovieReview
    {
        public int ID { get; set; }

        public int? MovieID { get; set; }

        public int? Score { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ReviewDate { get; set; }

        [Column(TypeName = "text")]
        public string Comment { get; set; }

        public virtual Movy Movy { get; set; }
    }
}
