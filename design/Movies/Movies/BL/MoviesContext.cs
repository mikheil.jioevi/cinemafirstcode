using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Movies.BL
{
    public partial class MoviesContext : DbContext
    {
        public MoviesContext()
            : base("name=MovieContext")
        {
        }

        public virtual DbSet<Director> Directors { get; set; }
        public virtual DbSet<MovieReview> MovieReviews { get; set; }
        public virtual DbSet<Movy> Movies { get; set; }
        public virtual DbSet<MoviesCategory> MoviesCategories { get; set; }
        public virtual DbSet<Nationality> Nationalities { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Director>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Director>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<MovieReview>()
                .Property(e => e.Comment)
                .IsUnicode(false);

            modelBuilder.Entity<Movy>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Movy>()
                .HasMany(e => e.MovieReviews)
                .WithOptional(e => e.Movy)
                .HasForeignKey(e => e.MovieID);

            modelBuilder.Entity<MoviesCategory>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<MoviesCategory>()
                .HasMany(e => e.Movies)
                .WithOptional(e => e.MoviesCategory)
                .HasForeignKey(e => e.MovieCategoryID);

            modelBuilder.Entity<Nationality>()
                .Property(e => e.Name)
                .IsUnicode(false);
        }
    }
}
