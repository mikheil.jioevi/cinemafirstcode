namespace Movies.BL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Movy
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Movy()
        {
            MovieReviews = new HashSet<MovieReview>();
        }

        public int ID { get; set; }

        public int? MovieCategoryID { get; set; }

        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        public int? DirectorID { get; set; }

        public int? DurationMinutes { get; set; }

        [Column(TypeName = "date")]
        public DateTime? YearOfRelease { get; set; }

        public virtual Director Director { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MovieReview> MovieReviews { get; set; }

        public virtual MoviesCategory MoviesCategory { get; set; }
    }
}
