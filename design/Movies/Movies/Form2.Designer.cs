﻿namespace Movies
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NewDirectorBTN = new System.Windows.Forms.Button();
            this.btn_GO_BACK = new System.Windows.Forms.Button();
            this.editCategoryNationalityBTN = new System.Windows.Forms.Button();
            this.AdminDataGrid = new System.Windows.Forms.DataGridView();
            this.AddCategoryNationality = new System.Windows.Forms.Button();
            this.AddMovieBTN = new System.Windows.Forms.Button();
            this.editDirectorBTN = new System.Windows.Forms.Button();
            this.editMovieBTN = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.AdminDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // NewDirectorBTN
            // 
            this.NewDirectorBTN.BackColor = System.Drawing.Color.PaleTurquoise;
            this.NewDirectorBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewDirectorBTN.Location = new System.Drawing.Point(379, 24);
            this.NewDirectorBTN.Name = "NewDirectorBTN";
            this.NewDirectorBTN.Size = new System.Drawing.Size(172, 75);
            this.NewDirectorBTN.TabIndex = 0;
            this.NewDirectorBTN.Text = "ADD Director";
            this.NewDirectorBTN.UseVisualStyleBackColor = false;
            this.NewDirectorBTN.Click += new System.EventHandler(this.NewDirectorBTN_Click);
            // 
            // btn_GO_BACK
            // 
            this.btn_GO_BACK.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_GO_BACK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GO_BACK.Location = new System.Drawing.Point(6, 357);
            this.btn_GO_BACK.Name = "btn_GO_BACK";
            this.btn_GO_BACK.Size = new System.Drawing.Size(130, 47);
            this.btn_GO_BACK.TabIndex = 1;
            this.btn_GO_BACK.Text = "GO BACK";
            this.btn_GO_BACK.UseVisualStyleBackColor = false;
            this.btn_GO_BACK.Click += new System.EventHandler(this.btn_GO_BACK_Click);
            // 
            // editCategoryNationalityBTN
            // 
            this.editCategoryNationalityBTN.BackColor = System.Drawing.Color.PaleTurquoise;
            this.editCategoryNationalityBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editCategoryNationalityBTN.Location = new System.Drawing.Point(12, 112);
            this.editCategoryNationalityBTN.Name = "editCategoryNationalityBTN";
            this.editCategoryNationalityBTN.Size = new System.Drawing.Size(243, 53);
            this.editCategoryNationalityBTN.TabIndex = 3;
            this.editCategoryNationalityBTN.Text = "edit category & nationality";
            this.editCategoryNationalityBTN.UseVisualStyleBackColor = false;
            this.editCategoryNationalityBTN.Click += new System.EventHandler(this.editCategoryNationalityBTN_Click);
            // 
            // AdminDataGrid
            // 
            this.AdminDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.AdminDataGrid.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.AdminDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AdminDataGrid.Location = new System.Drawing.Point(142, 273);
            this.AdminDataGrid.Name = "AdminDataGrid";
            this.AdminDataGrid.Size = new System.Drawing.Size(754, 131);
            this.AdminDataGrid.TabIndex = 5;
            // 
            // AddCategoryNationality
            // 
            this.AddCategoryNationality.BackColor = System.Drawing.Color.PaleTurquoise;
            this.AddCategoryNationality.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddCategoryNationality.Location = new System.Drawing.Point(12, 24);
            this.AddCategoryNationality.Name = "AddCategoryNationality";
            this.AddCategoryNationality.Size = new System.Drawing.Size(243, 75);
            this.AddCategoryNationality.TabIndex = 6;
            this.AddCategoryNationality.Text = "Add category & nationaliti";
            this.AddCategoryNationality.UseVisualStyleBackColor = false;
            this.AddCategoryNationality.Click += new System.EventHandler(this.AddCategoryNationality_Click);
            // 
            // AddMovieBTN
            // 
            this.AddMovieBTN.BackColor = System.Drawing.Color.PaleTurquoise;
            this.AddMovieBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddMovieBTN.Location = new System.Drawing.Point(724, 24);
            this.AddMovieBTN.Name = "AddMovieBTN";
            this.AddMovieBTN.Size = new System.Drawing.Size(172, 75);
            this.AddMovieBTN.TabIndex = 7;
            this.AddMovieBTN.Text = "ADD Movie";
            this.AddMovieBTN.UseVisualStyleBackColor = false;
            this.AddMovieBTN.Click += new System.EventHandler(this.AddMovieBTN_Click);
            // 
            // editDirectorBTN
            // 
            this.editDirectorBTN.BackColor = System.Drawing.Color.PaleTurquoise;
            this.editDirectorBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editDirectorBTN.Location = new System.Drawing.Point(379, 105);
            this.editDirectorBTN.Name = "editDirectorBTN";
            this.editDirectorBTN.Size = new System.Drawing.Size(172, 68);
            this.editDirectorBTN.TabIndex = 8;
            this.editDirectorBTN.Text = "Edit Director";
            this.editDirectorBTN.UseVisualStyleBackColor = false;
            // 
            // editMovieBTN
            // 
            this.editMovieBTN.BackColor = System.Drawing.Color.PaleTurquoise;
            this.editMovieBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editMovieBTN.Location = new System.Drawing.Point(724, 112);
            this.editMovieBTN.Name = "editMovieBTN";
            this.editMovieBTN.Size = new System.Drawing.Size(172, 68);
            this.editMovieBTN.TabIndex = 9;
            this.editMovieBTN.Text = "edit Movie";
            this.editMovieBTN.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(724, 186);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(172, 75);
            this.button1.TabIndex = 12;
            this.button1.Text = "Delete";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.PaleTurquoise;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(379, 184);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(172, 75);
            this.button2.TabIndex = 11;
            this.button2.Text = "Delete Director";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.PaleTurquoise;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(12, 184);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(243, 60);
            this.button3.TabIndex = 10;
            this.button3.Text = "Delete category & nationality";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.PaleTurquoise;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(6, 273);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(130, 47);
            this.button4.TabIndex = 13;
            this.button4.Text = "GO BACK";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(961, 416);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.editMovieBTN);
            this.Controls.Add(this.editDirectorBTN);
            this.Controls.Add(this.AddMovieBTN);
            this.Controls.Add(this.AddCategoryNationality);
            this.Controls.Add(this.AdminDataGrid);
            this.Controls.Add(this.editCategoryNationalityBTN);
            this.Controls.Add(this.btn_GO_BACK);
            this.Controls.Add(this.NewDirectorBTN);
            this.Name = "Admin";
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.Admin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AdminDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button NewDirectorBTN;
        private System.Windows.Forms.Button btn_GO_BACK;
        private System.Windows.Forms.Button editCategoryNationalityBTN;
        private System.Windows.Forms.DataGridView AdminDataGrid;
        private System.Windows.Forms.Button AddCategoryNationality;
        private System.Windows.Forms.Button AddMovieBTN;
        private System.Windows.Forms.Button editDirectorBTN;
        private System.Windows.Forms.Button editMovieBTN;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}