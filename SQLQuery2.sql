CREATE TABLE Nationalities (
	ID INT PRIMARY KEY IDENTITY(1,1),
    Name VARCHAR(255) NOT NULL
);

CREATE TABLE Directors (
    ID INT PRIMARY KEY IDENTITY(1,1),
    FirstName VARCHAR(255) NOT NULL,
    LastName VARCHAR(255) NOT NULL,
    NationalityID INT,
    FOREIGN KEY (NationalityID) REFERENCES Nationalities(ID)
);


CREATE TABLE MoviesCategories (
    ID INT PRIMARY KEY IDENTITY(1,1),
    Name VARCHAR(255) NOT NULL
);


CREATE TABLE Movies (
    ID INT PRIMARY KEY IDENTITY(1,1),
    MovieCategoryID INT,
    tTtle VARCHAR(255) NOT NULL,
    DirectorID INT,
    DurationMinutes INT,
    YearOfRelease INT,
    FOREIGN KEY (MovieCategoryID) REFERENCES MoviesCategories(ID),
    FOREIGN KEY (DirectorID) REFERENCES Directors(ID)
);

CREATE TABLE MovieReviews (
    ID INT PRIMARY KEY IDENTITY(1,1),
    MovieID INT,
    Score INT CHECK (Score BETWEEN 1 AND 10),
    ReviewDate DATE,
    Comment TEXT,
    FOREIGN KEY (MovieID) REFERENCES Movies(ID)
);